# CI Tools
These CI tools attempt to make development and release of software less painful.  Currently the following jobs are supported.

## node-all.yml
You can include several jobs by adding a configuration that references the `node-all.yml` file.

```yml
include:
   - project: yepreally/ci-tools
     ref: v1.0.0
     file:
       - node-all.yml
```

After including this, you will get support for:
- `node-build.yml` - **build** stage
  - Runs `npm ci` and the `npm run build` script and ensures it succeeds.  It also caches the result of `npm ci` so that the follow-on jobs can pull `node_modules` from the cache.
- `node-audit.yml` - **test** stage
  - Runs `npm audit`.  This will not fail the pipeline.
- `npm-outdated.yml` - **test** stage
  - Runs `npx npm-check-updates`.  This will not fail the pipeline
- `node-lint.yml` - **test** stage
  - Runs `npm run lint` (if present) and will fail the build if any lint errors.
- `node-jest.yml` - **test** stage
  - Runs `npm run test` (if present) and will fail the build if any tests fail. Also, this depends on the `jest-junit` reporter, so make sure this is installed.

## npm-publish.yml
Publish support can be added by including the `npm-publish.yml` file.

```yml
include:
   - project: yepreally/ci-tools
     ref: v1.0.0
     file:
       - node-all.yml
       - npm-publish.yml
```

When this is included, the following will occur.
1. Commits that are tagged will be built and published to `npmjs.com`.
2. Upon merge into the default branch, the prerelease version will be converted to a non prerelease version and tagged (which will then kick off the publish step mentioned in the previous line). For example, `v1.0.2-rc.3` will be bumped (using `npm version`) to `v1.0.2` and tagged.
3. If the current prerelease cannot be bumped and published, it will fail the `merge check` step and should fail the pipeline. This should prevent merging into the default branch unless the version can be published.

# Group Configuration

In order for modules to be published to npm, the `NPM_PUBLISH_TOKEN` needs to be set at the group level.  This is already done, but will require an update when the token expires.  If a publish fails, it could be that the token expired.

# Project Configuration

The publish scripts will not work unless each project is properly configured.  This includes creating a new SSH Key and a new Deploy Key.

## Add SSH Key

On your local machine, create an ssh key. This can be done by running: `ssh-keygen -f deploy_key -N "" -C ""`


Under `Settings > CI/CD > Variables` add variables for the public and private keys you just created.

Copy the contents of the generated `deploy_key` file into a new variable named `SSH_PRIVATE_KEY` with the following settings.
![private-key](images/private-key.png)

Copy the contents of the generated `deploy_key.pub` file into a new variable named `SSH_PUBLIC_KEY` with the following settings.
![public-key](images/public-key.png)


## Add Deploy Key

From `Settings > CI/CD > Variables` copy the value of the `SSH_PUBLIC_KEY` variable.

Under `Settings > Repository > Deploy keys` add a key with the following properties. Ensure that `Grant write permissions to this key` is checked or the jobs will fail.
![deploy-key](images/deploy-key.png)

That's it! Now when you merge changes into the default branch, your code should be re-versioned and published.  It is also recommended that you add release notes for this version/tag under `Deployments > Releases`.
